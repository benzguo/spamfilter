(* the different filtering algorithms we'll try, but for now there's just to be a naive Bayes *)
open Dict
open Helpers
open Crawler

class type filter_t =
object

  (* filename to bool *)
  method classify: string -> int*int -> bool

end

(*
crawler object will contain all three dictionaries--spam, ham, and probabilities
*)
class naiveBayes (c: crawler_t)  (t: float) : filter_t =
object(self)  
  (* takes top 15 probabilities and computes prob_spam and prob_ham, returns as 
    float tuple *)
  method private products (lst: float list) : float * float =
    (List.fold_left ( *.) 1. lst, 
      List.fold_left (fun r x -> (1. -. x) *. r) 1. lst)

  (* sorts tokens in descending order according to probablities in prob
     and returns top 15 *)
  method private sort_tokens (lst: string list) : float list =
    (* sorted in descending order *)
    let probs =
      List.fold_right 
        (fun x r -> if (c#get_probdict)#member x then 
	       (((c#get_probdict)#lookup x),x)::r else r) lst []
    in
    (* distance, token tuples sorted in descending order *)
    let distanceList =
      List.sort (fun x y -> 
        let (x1,_) = x in
        let (x2,_) = y in
        if x1 = x2 then 0 
        else if x1 < x2 then 1 
        else (0-1))
      (List.map (fun x -> let (v,t) = x in (abs_float (v -. 0.5),t)) probs)
    in
    let rec take_top (old: 'a list) (newlst: 'b list) (count: int) : 'b list =
      if count <= 0 then newlst else
	     (match old with
	         |[] -> newlst
	         |hd::tl -> let (v,t) = hd in
              take_top tl (((c#get_probdict)#lookup t)::newlst) (count-1))
    in
    take_top distanceList [] 15

  (* true for spam, false for ham *)
  method classify (filename: string) (ngrams: int*int) : bool =
    let tokens = Helpers.gen_ngrams (Helpers.parse_file filename) ngrams in
    let (pspam, pham) = self#products (self#sort_tokens tokens) in
    (pspam /. (pspam +. pham)) > t
end

class chisquare (c: crawler_t) (t:float) : filter_t =
object(self)

  method private get_probs (lst: string list):(float*string)list =
    List.fold_right 
        (fun x r -> if (c#get_probdict)#member x then 
	       ((c#get_probdict)#lookup x,x)::r else r) lst []

  (*calculates degree of belief used in Chi-square distribution method.
  strength of prior beliefs = 1, probability assigned to new words = .5*)
  method private map_degree_of_belief (lst:(float*string)list)
                                            : (float list) * (float list) =
    let s = List.map 
    (fun (x,v) -> let count = (if (c#get_spamcountdict)#member v
          then (c#get_spamcountdict)#lookup v else 0.) in
      ((1. *. 0.5) +. (count *. x))/.(1. +. count)) lst in
    let h = List.map (fun n -> 1. -. n) s in
    (s, h)

  method private getI (fs: float list * float list) : float =
    let (s, h) = fs in
    let d = List.length s in
    let df = float_of_int (d - (d mod 2)) in
    let rec getchi (fl: float list) (b: float) =
      match fl with
        | f::tl -> getchi tl (b +. (log f))
        | [] -> b in
    let schi = (-2.) *. (getchi s 0.) in
    let hchi = (-2.) *. (getchi h 0.) in
    let rec getp (n: int) (chi: float) (df: float) (b: float) : float =
      if float_of_int n <= df then
        getp (n+1) chi df (b +. (b *. ((chi/.(2.))/.(float_of_int n))))
      else 
        b in
    let ps = min 1. (exp(-.schi/.(2.)) *. (getp 1 schi df 1.)) in
    let ph = min 1. (exp(-.hchi/.(2.)) *. (getp 1 hchi df 1.)) in
    (1. +. ps -. ph)/.(2.)

   method classify (filename: string) (ngrams: int*int) : bool =
    let tokens = Helpers.gen_ngrams (Helpers.parse_file filename) ngrams in
   t < self#getI (self#map_degree_of_belief (self#get_probs tokens))

end
