open Dict
open Helpers

let dictsize = 10000
let print_dir = false

class type crawler_t =
object
  (* add a list of tokens to a dictionary *)
  (* add all files in a directory to either spamdict or hamdict *)
  method add_spam : string -> int*int -> unit
  method add_ham : string -> int*int -> unit
  method add_spamcount : string -> int*int -> unit
  (* merges spamdict and hamdict, returning a probdict *)
  method merge_dicts : unit -> unit
  (* methods to access instance variables *)
  method get_spamdict : dictionary_t
  method get_hamdict : dictionary_t
  method get_probdict : dictionary_t
  
  method get_spamcountdict : dictionary_t
  method get_spams_read : int
  method get_hams_read : int

end

class crawler : crawler_t = 
  object (self)

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  val spamdict = new hash_dictionary dictsize
  val hamdict = new hash_dictionary dictsize
  val probdict = new hash_dictionary dictsize
  val spamcountdict = new hash_dictionary dictsize
  val mutable read =0
  val mutable spams_read = 0
  val mutable hams_read = 0
  val mutable token_list = []

  method get_spamdict = spamdict
  method get_hamdict = hamdict 
  method get_probdict = probdict
  method get_spamcountdict = spamcountdict
  method get_spams_read = spams_read
  method get_hams_read = hams_read

  (*******************************)
  (**** Spam/Ham Dict Methods ****)
  (*******************************)

  method add_spam (directory: string) (ngrams: int*int): unit =
    (* create tokens *)
    Helpers.directory_map directory (fun dirname ->
    let tokens = Helpers.gen_ngrams (Helpers.parse_file dirname) ngrams in
    token_list <- tokens@token_list; read <- read+1);
      
    (* add tokens *)
    List.iter (fun a -> if spamdict#member a 
               then spamdict#replace a ((spamdict#lookup a) +. 1.0)
               else spamdict#insert a 1.0) token_list;
    (* update spams_read *)
    (spams_read <- read);
    (* re-initialize mutalbes *)
    (read <- 0);
    token_list <- []
    
  method add_ham (directory: string) (ngrams: int*int): unit =
    (* create tokens *)
    Helpers.directory_map directory (fun dirname ->
      let tokens = Helpers.gen_ngrams (Helpers.parse_file dirname) ngrams in
      token_list <- tokens@token_list; read <- read+1);

    (* add tokens *)  
    List.iter (fun a -> if hamdict#member a 
               then hamdict#replace a ((hamdict#lookup a) +. 1.0)
               else hamdict#insert a 1.0) token_list;
    (* update hams_read *) 
    (hams_read <- read);
    (* re-initialize mutalbes *)
    (read <- 0);
    token_list <- []
    
  method add_spamcount (directory: string) (ngrams: int*int): unit =
    (* remove duplicate function *)
    let remove_dupes (lst: 'a list) : 'a list =
        List.fold_left (fun y x -> if List.exists (fun z -> (z = x)) y 
        then y else x::y) [] lst in
    (* add tokens *)
    Helpers.directory_map directory (fun dirname ->
      let token = Helpers.gen_ngrams (Helpers.parse_file dirname) ngrams in
      let token_rd = remove_dupes token in
      token_list <- token_rd@token_list);
      
    (* add non-duplicate tokens *)      
    List.iter (fun a -> if spamcountdict#member a 
               then spamcountdict#replace a ((spamdict#lookup a) +. 1.0)
               else spamcountdict#insert a 1.0) token_list;
    (* re-initialize mutable *)
    token_list <- []


  (*******************************)
  (**** ProbDict Methods *********)
  (*******************************)
  method merge_dicts (unit) =
    let hamlist = hamdict#test () in
    let spamlist = spamdict#test () in
    (* makedict begins the merge process by going through the ham list,
       finding words in common with spam list, calculating the probabilities,
       adding them to probdict, and then also finding the words unique to ham
       and adding them with probability 0.01.*)
    let rec makedict (ls: (string * float) list) : unit =
      match ls with
        | [] -> ()
        | (hamt, hamv)::tl ->
          if spamdict#member hamt then
            let spamv= spamdict#lookup hamt in
            let probspam = (spamv)/.(spamv +. hamv) in
              (if probspam < 0.01 then probdict#insert hamt 0.01
              else if probspam > 0.99 then probdict#insert hamt 0.99
              else probdict#insert hamt probspam);
              makedict tl
          else (probdict#insert hamt 0.01 ; makedict tl) 
    in

    (*makedict2 finishes the merge by going through the spam list and finding
      the remaining words unique to spam and adding them to probdict with a
      probability 0.99*)
    let rec makedict2 (ls: (string * float) list) : unit =
      match ls with
        | [] -> ()
        | (spamt, spamv)::tl ->
          if not(hamdict#member spamt) then (probdict#insert spamt 0.99 ; 
            makedict2 tl)
          else makedict2 tl 
    in
    (makedict hamlist; makedict2 spamlist)

end

