class type dictionary_t = 
object

  (* Returns true if and only if the key is in the dictionary. *)
  method member : string -> bool

  (* Returns as an option the value associated with the provided key. If
   * the key is not in the dictionary, return None. *)
  method lookup : string -> float

  (* Inserts a (key,value) pair float our dictionary. If the key is already
   * in our dictionary, update the key to have the new value. *)
  method insert : string -> float -> unit

  (* Removes the given key from the dictionary. If the key is not present,
   * return the original dictionary. *)
  method remove : string -> unit

  (* Replace the current binding of the given key in the dictionary. 
    If the key is not present, add new binding *)
  method replace : string -> float -> unit

  (* folds over the dictionary *)
  method fold : (string -> float -> float -> float) -> float -> float

  (* returns a list of all key value pairs *)
  method test : unit -> (string * float) list

end

class hash_dictionary (size: int) : dictionary_t =
 object (this)

  (******************************)
  (***** Instance Variables *****)
  (******************************)

   val dict = Hashtbl.create size

  (**************************)
  (********* Methods ********)
  (**************************)

   method member (k: string) = Hashtbl.mem dict k

   (* returns current binding of k in hashtable *)
   method lookup (k: string) = Hashtbl.find dict k

   method insert (k: string) (v: float) = Hashtbl.add dict k v

   (* removes current bindings of k *)
   method remove (k: string) = Hashtbl.remove dict k

   method replace (k: string) (v: float) = Hashtbl.replace dict k v

   method fold (f: string -> float -> float -> float) (base: float) 
     = Hashtbl.fold f dict base

   method test () = Hashtbl.fold (fun t v r -> (t,v)::r ) dict []

 end


