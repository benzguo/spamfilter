open Dict
open Helpers
open Crawler
open Filter

type 'a toggle = New of 'a | Old of 'a
let debug = ref false
let ngrams = ref (New (3,0))
let spamtest_dir = ref (".."^Filename.dir_sep^"data"^Filename.dir_sep^"testspam")
let hamtest_dir = ref (".."^Filename.dir_sep^"data"^Filename.dir_sep^"testham")
let ham_dir = ref (New (".."^Filename.dir_sep^"data"^Filename.dir_sep^"trainingham"))
let spam_dir = ref (New (".."^Filename.dir_sep^"data"^Filename.dir_sep^"trainingspam"))
let chi_s = ref (New false)
let train_crawler = ref None
let f1 = ref None

(* cmd line args [DEPRECATED - we're using the program loop below] 
let arg_spec = [
	"-s", Arg.Set_string spamtest_dir, "\"spam test directory\"";
	"-h", Arg.Set_string hamtest_dir, "\"ham test directory\"";
	"-ht", Arg.Set_string ham_dir, "\"ham training directory\"";
	"-st", Arg.Set_string spam_dir, "\"spam training directory\"";
	"-debug", Arg.Set debug, "**include to enable debug**\n";
]

let usage_msg = "usage: ./filter\n\n**followed by**"

let usage _ = Arg.usage arg_spec usage_msg; exit 1
*)

let extract v =
	match v with
		| New a -> a
		| Old a -> a

let is_new v =
	match v with
		| Old _ -> false
		| New _ -> true

let classify_strings filename =
	let ans =  if (Helpers.force_deop !f1)#classify filename (extract !ngrams)
	then "Spam" else "Ham" in print_endline (filename ^ "= "^ans)

(* prepares the dictionaries in a crawler object *)
let train (c: crawler_t) spamdir hamdir =
	(* add timers to functions*)
	let timed_add_spam = Helpers.time_fun2 c#add_spam in
	let timed_add_ham = Helpers.time_fun2 c#add_ham in
	let timed_add_sc = Helpers.time_fun2 c#add_spamcount in
	let timed_merge = Helpers.time_fun c#merge_dicts in
	(* add spam *)
	print_string("-------------------\n");
	flush_all ();
    let (_, add_spam_t) = timed_add_spam spamdir (extract !ngrams) in
	print_string ("added "^(string_of_int c#get_spams_read)^" spams in "^
		(string_of_float add_spam_t)^"s\n");
	flush_all ();
	(* add ham *)
    let (_, add_ham_t) = timed_add_ham hamdir (extract !ngrams) in
	print_string ("added "^(string_of_int c#get_hams_read)^" hams in "^
		(string_of_float add_ham_t)^"s\n");
	flush_all ();
	(* add spamcount *)
	if extract !chi_s then(
        let (_, add_spamcount_t) = timed_add_sc spamdir (extract !ngrams) in
	    print_string ("added spamcount in "^
		    (string_of_float add_spamcount_t)^"s\n");
	    flush_all ()
	);
	(* generate probdict *)
	let (_, merge_t) = timed_merge () in
	print_string ("merged in "^(string_of_float merge_t)^"s\n");
	flush_all ();

	(* optional: print dictionaries *)
	if !debug then( 
		print_string "\n\n****SPAMDICT****\n\n";
		ignore(List.map (Helpers.print_tuple !debug) ((c#get_spamdict)#test ()));
		print_string "\n\n****HAMDICT****\n\n";
		ignore(List.map (Helpers.print_tuple !debug) ((c#get_hamdict)#test ()));
		print_string "\n\n****PROBDICT****\n\n";
		ignore(List.map (Helpers.print_tuple !debug) ((c#get_probdict)#test ())))

let main () =
	(* spam and ham directories *)
	let spamdir = String.concat "" 
		[(extract !spam_dir); Filename.dir_sep] 
	in
	let hamdir = String.concat "" 
		[(extract !ham_dir); Filename.dir_sep] 
	in
	let spamtestdir = String.concat "" 
		[!spamtest_dir; Filename.dir_sep]
	in
	let hamtestdir = String.concat "" 
		[!hamtest_dir; Filename.dir_sep]
	in

	(* make a crawler *)
	let _ = (if (!train_crawler = None || 
		(is_new !ham_dir) || (is_new !spam_dir)|| is_new !chi_s || is_new !ngrams)
	then 
		(train_crawler := Some (new crawler); train (Helpers.force_deop 
			(!train_crawler)) spamdir hamdir)) in

	(* add timer to directory_map *)
	let timed_map = Helpers.time_fun2 Helpers.directory_map in

	(* optional: print dictionaries *)
	let _ = if !debug then( 
		print_string "\n\n****SPAMDICT****\n\n";
		ignore(List.map (Helpers.print_tuple !debug)
			(((Helpers.force_deop !train_crawler)#get_spamdict)#test ()));
		print_string "\n\n****HAMDICT****\n\n";
		ignore(List.map (Helpers.print_tuple !debug)(
			((Helpers.force_deop !train_crawler)#get_hamdict)#test ()));
		print_string "\n\n****PROBDICT****\n\n";
		ignore(List.map (Helpers.print_tuple !debug)
			(((Helpers.force_deop !train_crawler)#get_probdict)#test ())))
	in

	(* refs for results *)
	let spam_results = ref [] in
	let ham_results = ref [] in

	(* make a filter *)
	let threshold = Helpers.get_float () in
	let _ = f1 := 
	    if (extract !chi_s) then Some 
	    (new chisquare (Helpers.force_deop !train_crawler) threshold)
	    else Some (new naiveBayes (Helpers.force_deop !train_crawler) threshold) in

	(* classify spam directory *)
	let (_, class_spam_t) = timed_map spamtestdir 
		(fun x -> spam_results := ((Helpers.force_deop !f1)#classify x (extract !ngrams))
				:: (!spam_results)) in
	let classified_spams = (List.length !spam_results) in
	print_string ("classified "^(string_of_int classified_spams)^" spams in "^
		(string_of_float class_spam_t)^"s\n");
	flush_all ();

	(* classify ham directory *)
	let (_, class_ham_t) = timed_map hamtestdir 
		(fun x -> ham_results := ((Helpers.force_deop !f1)#classify x (extract !ngrams)):: 
			(!ham_results)) in
	let classified_hams = (List.length !ham_results) in
	print_string ("classified "^(string_of_int classified_hams)^" hams in "^
		(string_of_float class_ham_t)^"s\n");
	flush_all ();
	
	(*print results*)
	Helpers.print_results !spam_results !ham_results threshold
	classified_spams classified_hams

	(* update old spam/ham dicts to new values to reflect new crawler obj*)
	let _ = (spam_dir := Old (extract !spam_dir); ham_dir := Old (extract !ham_dir);
	chi_s := Old (extract !chi_s); ngrams := Old (extract !ngrams))

	;; 

(* program loop *)
let rec loop () : unit =              
	print_endline ("-------------------\n"^
	"Welcome!\n"^
	"(r)un\n"^
	"(e)dit\n"^
	"(q)uit\n"^
	"(h)elp");
	(if !f1 <> None then print_endline "(c)lassify directory");
	print_string ">> ";
	match read_line () with
		| "r" -> main (); loop ()
		| "e" -> edit_loop ()
		| "q" -> ()	
		| "h" -> print_endline ("-------------------\n"^
			"r: run program\ne: edit variables\nq: quit program"); loop ()
		| "c" -> if !f1 <> None then 
				Helpers.directory_map (Helpers.get_dir "new spam directory: ") 
					classify_strings; loop ()
		| _ -> print_endline "that's not an option! Try again \n"; loop ()


and edit_loop () =
	(* for printing state of ngram and chi-squared options *)
	let ngramstr = let (n1, n2) = extract !ngrams in 
                if n2 = 0 then string_of_int(n1)^"-grams"
                else string_of_int(n1)^"- and "^string_of_int(n2)^"- grams" in
	let chi_str = if extract !chi_s then "enabled" else "disabled" in

    (* gets ngram values from user *)
    (* The user can choose either 1 or 2 ngrams, e.g. 1-grams, or 2- and 3-grams *)
    let rec get_n (unit) : int*int =
        print_string "Enter first value (must be non-zero): ";
        let n1 = read_int () in 
        print_string "Enter second value (enter 0 if none): ";
        let n2 = read_int () in
        if n1 > 0 && n2 >= 0 then (n1, n2)
        else
        (print_endline "Invalid values. First value must exceed 0,
        	\nsecond value must exceed or equal 0";
        get_n ()) in
                
	print_endline ("-------------------\n"^
		"which variable to edit?\n Note: ./ is current directory,"^
		" ../ is parent directory \n"^
		"(s)pam         = "^(extract !spam_dir)^"\n"^
		"(h)am          = "^(extract !ham_dir)^"\n"^
		"(st) spam test = "^(!spamtest_dir)^"\n"^
		"(ht) ham test  = "^(!hamtest_dir)^"\n"^
		"(n)grams       = "^ngramstr^"\n"^
		"(c)hi squared  = "^chi_str^"\n"^
		"(d)ebug = "^(string_of_bool !debug)^"\n"^
		"(b)ack\n(q)uit");
	print_string ">> ";
	match read_line () with
		| "s" ->
		spam_dir := New (Helpers.get_dir "new spam directory: "); edit_loop ()
		| "h" -> 
		ham_dir := New (Helpers.get_dir "new ham directory: "); edit_loop ()
		| "st" -> 
		spamtest_dir := Helpers.get_dir "new spam test directory: "; edit_loop ()
		| "ht" -> 
		hamtest_dir := Helpers.get_dir "new ham test directory: "; edit_loop ()
		| "n" -> ngrams := New (get_n ()); 
			print_endline "changed! Redirecting..."; 
		edit_loop ()
		| "c" -> chi_s := New (not (extract !chi_s)); 
			print_endline "changed! Redirecting..."; 
		edit_loop ()
		| "d" -> print_string "type 0 for false and 1 for true"; 
			(match read_int () with
			|0 -> debug := false; edit_loop ()
			|1 -> debug := true; edit_loop ()
			|_ -> print_endline "that's not an option! Try again \n"; edit_loop ())
		| "b" -> loop ()
		| "q" -> ()
		| _ -> print_endline "that's not an option! Try again \n"; edit_loop ()
;;


loop ()


