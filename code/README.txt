README
*************************
** Compiling / running **
*************************
1) make clean
	-Clears directory and eliminates “._” files
2) make
	- Compiles files
3) ./filter
	- runs the program. 
4) follow the prompts
	- enter ‘h’’ for help 
Notes: 
- if entering a directory that is not inside the same directory as the executable, please enter full filepath - the program will reject strings that aren’t directories.
- in order to classify a custom directory, first run the program once to generate dictionaries. to classify, enter 'c'. you'll need to enter a directory you want to explicitly classify. We suggest using 
../data/mystery

***********
** code  **
***********
Crawler.ml
	-Contains code for the crawler object that stores the dictionaries of tokens and probabilities
Dict.ml
	-Contains code for the dict object that the crawler holds, implemented with the OCaml Hashtbl module
Filter.ml
	- Contains the interface for filter objects and two implementations, naive bayes and chi square.
Helpers.ml
	-Contains various helper functions like parse_file, directory_map, ngrams, and timing functions.
Main.ml
	-Main program loop and main function, includes user interface in the form of a double loop and some read_line statements
Makefile
	- Compiles the files in the right order and provides instructions for cleaning the directory
ResultsLog.txt
	- A log of most our results over time detailing our progress in achieving 99.6% accuracy

**********
** data **
**********
See data/CorporaDetails.txt for more information.