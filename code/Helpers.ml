let print_tokens = false

(* generic helpful functions *)
module Helpers =
struct

	let directory_map (dir: string) (f: string -> unit): unit =
	    let rec recursive_map dir f =
	      (* eliminate files starting with ._ because wtf are those*)
	      let files = List.filter (fun x -> not(String.get x 0 = '.')) 
	      (Array.to_list (Sys.readdir dir)) in
	      let rec loop (fs: string list) =
	        match fs with
	          | [] -> ()
	          | hd::tl ->
	            let dirname =  String.concat "" [dir; Filename.dir_sep; hd] in
	            (* note: string indexing starts at 0 *)
	            if Sys.is_directory dirname
	            then 
	              (try recursive_map dirname f with _ -> (); loop tl)
	            else f dirname ; loop tl
	      in 
	      loop files
	    in
	    recursive_map dir f

	let force_deop v =
		match v with
			| None -> failwith "Cannot call deoptionalize on None"
			| Some a -> a

	(* returns function to generate ngrams of desired length *)
	let rec gen_ngram (n: int) (tokens: string list) : string list =
	    let rec loop1 (n:int) (n':int) (tks: string list) : string list =
	        let rec loop2 (s: string list) (acc: string list) (ngram: string list) (c: int)=
			    let c = c + 1 in
		        (match s with
		          | [] -> acc
		          | hd::tl ->
		          	if c = n then loop2 tl 
		          		((String.concat " " (hd::ngram))::acc) [] 0
		          	else loop2 tl acc (hd::ngram) c) in
		if n' > 1 then
		(loop2 tks [] [] 0)@ (loop1 n (n'-1) (List.tl tks))
		else loop2 tks [] [] 0 in
    loop1 n n tokens;;

	(* generates ngrams given a tuple *)
	(* e.g. gen_ngrams tokens (2,3) generates bigrams and trigrams *)
	let gen_ngrams (tokens: string list) (vals: int*int) : string list =
          let (n1, n2) = vals in if n2 = 0
          then
            gen_ngram n1 tokens
          else
	    (gen_ngram n1 tokens)@(gen_ngram n2 tokens)

	(* gets a directory from user, making sure the directory exists *)
	let rec get_dir (prompt: string) : string =
		print_string prompt;
		let d = read_line () in
		try if (Sys.is_directory d) then d else 
			(print_endline "Not a directory, try again.";
			get_dir prompt)
		with _ -> (print_endline "Not a directory, try again.";
			get_dir prompt)

	let rec get_float () : float =
		print_string "Enter threshold probability for spam: ";
		let v = read_float () in
		if not(v > 0. && v < 1.) then (print_endline "Invalid input, try again.";
			get_float ()) else v
	
	(* open a file and generate a string list of tokens *)
	(* does some token cleaning, see reg ex *)
	let parse_file (file: string) : string list =
	    let tokens = ref [] in
	    let chan = open_in file in
	    try
	      while true; do
	        tokens := (Str.split (Str.regexp "[ \t| <(,;\t | \t>)]+") 
	    		 (input_line chan))@(!tokens)
	      done; []
	    with End_of_file ->
	      close_in chan;
	      !tokens

	(* for printing dictionaries *)
	let print_tuple debug x=
	    if debug then
		let (t,v) = x in
		print_string ("token: " ^ t ^ " ");
		print_float v; 
		print_string "\n"

	(* print results of filtering *)
	let print_results (spams: bool list) (hams: bool list) threshold 
		c_spams c_hams: unit = 
		let d_spams = (List.length (List.filter (fun x-> x) spams)) in
		let d_hams = (List.length (List.filter (fun x-> x) hams)) in
		let acc_spam = (float d_spams) /. (float c_spams) in
		let fpr = (float d_hams) /. (float c_hams) in
		print_string("-------------------\n");
		print_string("***** RESULTS *****\n");
		print_endline ("Threshold: "^(string_of_float (threshold *. 100.))^"%");
		print_string("Spam detected = "^string_of_int(d_spams)^"/"^
			string_of_int(c_spams)^"\n");
		print_string("Accuracy = "^string_of_float(acc_spam*.100.)^"%\n");
		print_string("Hams as spams = "^
		string_of_int(d_hams)^"/"^
		string_of_int(c_hams)^"\n");
		print_string("False positive rate = "^
			string_of_float(fpr*.100.)^"%\n");
		print_string("-------------------\n\n\n")

	(* time_fun f returns a function which when run on its argument,
	 * calculates the time (in seconds) for the function to run and
	 * returns that time as a floating point number. From Lecture 6 *)
    let time_fun f = 
		fun x -> 
		    let t0 = Unix.time() in 
		    let ans = f x in 
		    let t1 = Unix.time() in 
		      ans, (t1 -. t0)

  	(* time_fun2 f returns a function which when run on two arguments,
	 * calculates the time (in seconds) for the function to run and
	 * returns that time as a floating point number. *)
    let time_fun2 f = 
		fun x y -> 
		    let t0 = Unix.time() in 
		    let ans = f x y in 
		    let t1 = Unix.time() in 
		      ans, (t1 -. t0)	
	
end
